<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=1024" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <title>Immutable.js Crash Course | The What, Why, and How of Immutable state in React | by David Zwarg dzwarg@massmutual.com</title>

    <meta name="description" content="The What, Why, and How of Immutable state in React" />
    <meta name="author" content="David Zwarg" />

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:regular,semibold,italic,italicsemibold|PT+Sans:400,700,400italic,700italic|PT+Serif:400,700,400italic,700italic" rel="stylesheet" />
    <link href="css/impress-demo.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/default.css">

    <link rel="shortcut icon" href="favicon.png" />
    <link rel="apple-touch-icon" href="favicon.png" />
</head>

<body class="impress-not-supported">

<div class="fallback-message">
    <p>Your browser <b>doesn't support the features required</b> by impress.js, so you are presented with a simplified version of this presentation.</p>
    <p>For the best experience please use the latest <b>Chrome</b>, <b>Safari</b> or <b>Firefox</b> browser.</p>
</div>

<!--

    Now that's the core element used by impress.js.

    That's the wrapper for your presentation steps. In this element all the impress.js magic happens.
    It doesn't have to be a `<div>`. Only `id` is important here as that's how the script find it.

    You probably won't need it now, but there are some configuration options that can be set on this element.

    To change the duration of the transition between slides use `data-transition-duration="2000"` giving it
    a number of ms. It defaults to 1000 (1s).

    You can also control the perspective with `data-perspective="500"` giving it a number of pixels.
    It defaults to 1000. You can set it to 0 if you don't want any 3D effects.
    If you are willing to change this value make sure you understand how CSS perspective works:
    https://developer.mozilla.org/en/CSS/perspective

    But as I said, you won't need it for now, so don't worry - there are some simple but interesting things
    right around the corner of this tag ;)

-->
<div id="impress">

    <!--

        Here is where interesting thing start to happen.

        Each step of the presentation should be an element inside the `#impress` with a class name
        of `step`. These step elements are positioned, rotated and scaled by impress.js, and
        the 'camera' shows them on each step of the presentation.

        Positioning information is passed through data attributes.

        In the example below we only specify x and y position of the step element with `data-x="-1000"`
        and `data-y="-1500"` attributes. This means that **the center** of the element (yes, the center)
        will be positioned in point x = -1000px and y = -1500px of the presentation 'canvas'.

        It will not be rotated or scaled.

    -->
    <div class="step" data-x="0" data-y="0">
        <h1>Immutable.js</h1>
        <h2>Crash Course</h2>
    </div>

    <div class="step" data-x="2000" data-y="0">
        <h1>What is Immutable.js?</h1>
    </div>

    <div id="what" class="step" data-x="2000" data-y="1000">
        <p>A library for efficiently manipulating immutable data structures.</p>
    </div>

    <div id="what-library" class="step what" data-x="2000" data-y="1300">
        <div class="term">
            <p>library</p>
        </div>
        <p>Consumed via <b>import</b> or <b>require</b>:</p>
        <pre>// ES6
import * from 'immutable';
// ES5
var immutable = require('immutable');
</pre>
    </div>

    <div id="what-efficient" class="step what" data-x="2000" data-y="1300">
        <div class="term">
            <p>efficiently</p>
        </div>
        <p>Structural sharing for derived immutable objects.</p>
        <pre>// The only memory delta is for the
// 'flag' key - the remainder of prevState
// is returned unchanged.
let newState = prevState.set('flag', true);</pre>
    </div>

    <div id="what-manipulate" class="step what" data-x="2000" data-y="1300">
        <div class="term">
            <p>manipulating</p>
        </div>
        <p>Modify keys and subkeys of a collection:</p>
        <pre>const state0 = new Map({"flag": false})
// modify a property of `state0`
const state1 = state0.set("flag",
    {"dir": "up"});
// modify a property of `state1.flag`
const state2 = state1.setIn(
    ["flag", "dir"], "down");</pre>
    </div>

    <div id="what-immutable" class="step what" data-x="2000" data-y="1300">
        <div class="term">
            <p>immutable</p>
        </div>
        <p>Modification of an immutable object returns a new immutable object.</p>
        <pre>const state0 = new Map({"flag": true});
const state1 = state0.set("flag", false);
expect(state0).not.to.equal(state1);</pre>
    </div>

    <div id="what-structure" class="step what" data-x="2000" data-y="1300">
        <div class="term">
            <p>data structures</p>
        </div>
        <p>The base <b>Collection</b> is extended by:
        <pre>import {List, Map, Set, Stack, Seq}
    from 'immutable';</pre>
    </div>

    <div id="why" class="step why" data-x="4000" data-y="0">
        <h1>Why Use Immutable.js?</h1>
    </div>

    <div id="why-performance-1" class="step why" data-x="4000" data-y="1000">
        <p>
            Immutable.js combined with Redux provides excellent performance.
            <br/>
            <br/>
            Redux's default re-rendering checks optimize for immutable objects
            (via <a href="https://github.com/reactjs/react-redux/blob/master/docs/api.md" target="_blank">connect</a>)
        </p>
    </div>

    <div id="why-performance-2" class="step why" data-x="4000" data-y="2000">
        <p>Immutable.js is extremely efficient with large data structures by using <b>structural sharing</b>.</p>
    </div>

    <div id="why-pattern" class="step why" data-x="4000" data-y="3000">
        <p>React already enforces immutable objects as outputs of reducers.</p>
    </div>

    <div id="how" class="step how" data-x="6000" data-y="0">
        <h1>How To Use Immutable.js</h1>
    </div>

    <div id="how-separate" class="step how" data-x="6000" data-y="1000">
        <p>Design your application as two discrete parts:</p>
        <ol>
            <li><b>data flow</b></li>
            <li><b>presentation</b></li>
        </ol>
    </div>

    <div id="how-state" class="step how" data-x="6000" data-y="2000">
        <p>Define your entire application state as a single Immutable object.</p>
        <pre>import {fromJS} from 'immutable';
export default fromJS({
    "id": "deadbeef",
    "name": {
        "first": "Randall",
        "last": "Stephens"
    }
})</pre>
    </div>

    <div id="how-dumb" class="step how" data-x="6000" data-y="3000">
        <p>Build all components as "dumb" components:</p>
        <pre>export default ({title}) =&gt; (&lt;div&gt;
    &lt;h1&gt;{title}&lt;/h1&gt;
&lt;/div&gt;);</pre>
    </div>

    <div id="how-smart" class="step how" data-x="6000" data-y="4000">
        <p>Build all containers as "smart" components that filter state:</p>
        <pre>const mapStateToProps = (state) =&gt;
    state.withMutations(map =&gt; {
        map.merge(state.filter(/* ... */))
            .merge(state.filter(/* ... */));
    });
    </div>

    <div id="how-connect" class="step how" data-x="6000" data-y="5000">
        <p>Transform immutable data into simple JavaScript objects only when needed:</p>
        <pre>export default connect(mapStateToProps)
    (toJS(DumbComponent));</pre>
        <p>See: <a href="https://redux.js.org/docs/recipes/UsingImmutableJS.html#use-a-higher-order-component-to-convert-your-smart-components-immutablejs-props-to-your-dumb-components-javascript-props">toJS implementation</a>
            in the Redux docs.
        </p>
    </div>

    <div id="core" class="step core" data-x="8000" data-y="0">
        <h1>More in Core</h1>
        <h2><a href="https://bitbucket.org/m38io/component.calc.core">component.calc.core</a></h2>
    </div>

    <div id="core-what" class="step core" data-x="8000" data-y="1000">
        <p><a href="https://bitbucket.org/m38io/component.calc.core">component.calc.core</a> is an npm module which builds on top of these patterns.</p>
    </div>

    <div id="core-actions" class="step core" data-x="8000" data-y="2000">
        <p>Drastically reduce actions. Just bring thunks. Baked in action creators:</p>
        <ul>
            <li><q>assocIn</q></li>
            <li><q>assocValidateIn</q></li>
            <li><q>reset</q></li>
        </ul>
    </div>

    <div id="core-reducers" class="step core" data-x="8000" data-y="3000">
        <p>Elimination of reducers, as baked in actions have baked in reducers.</p>
    </div>

    <div id="core-validation" class="step core" data-x="8000" data-y="4000">
        <p>Elements of state can be constrained:</p>
        <ul>
            <li><q>core.parse</q></li>
            <li><q>core.convert</q></li>
            <li><q>validate</q></li>
        </ul>
    </div>

    <div id="core-relations" class="step core" data-x="8000" data-y="5000">
        <p>Elements of state can be <i>related</i> to each other via a list of <q>relations</q>.</p>
        <pre>{
  "rate": {
    "relations": [{
      "path": ["percentage"],
        "relationFn": (val, prop, state) =&gt;
          val * 100
    }]
  }
}</pre>
    </div>

    <!--

        So to summarize of all the possible attributes used to position presentation steps, we have:

        * `data-x`, `data-y`, `data-z` - they define the position of **the center** of step element on
            the canvas in pixels; their default value is 0;
        * `data-rotate-x`, `data-rotate-y`, 'data-rotate-z`, `data-rotate` - they define the rotation of
            the element around given axis in degrees; their default value is 0; `data-rotate` and `data-rotate-z`
            are exactly the same;
        * `data-scale` - defines the scale of step element; default value is 1

        These values are used by impress.js in CSS transformation functions, so for more information consult
        CSS transfrom docs: https://developer.mozilla.org/en/CSS/transform

    -->
    <div id="overview" class="step" data-x="4000" data-y="2500" data-z="1000" data-scale="5">
        <h1>Thank you!</h1>
        <ul>
            <li>dzwarg@massmutual.com</li>
            <li>@dzwarg</li>
        </ul>
    </div>

</div>

<!--

    Last, but not least.

    To make all described above really work, you need to include impress.js in the page.
    I strongly encourage to minify it first.

    In here I just include full source of the script to make it more readable.

    You also need to call a `impress().init()` function to initialize impress.js presentation.
    And you should do it in the end of your document. Not only because it's a good practice, but also
    because it should be done when the whole document is ready.
    Of course you can wrap it in any kind of "DOM ready" event, but I was too lazy to do so ;)

-->
<script src="js/impress.js"></script>
<script>
impress().init();

// add highlight.js
addEventListener('load', function() {
  var allPre = document.querySelectorAll('pre');
  allPre.forEach(function (pre) {
      var worker = new Worker('worker.js');
      worker.onmessage = function(event) { pre.innerHTML = event.data; }
      worker.postMessage(pre.textContent);
  })
})
</script>

<!--

    The `impress()` function also gives you access to the API that controls the presentation.

    Just store the result of the call:

        var api = impress();

    and you will get three functions you can call:

        `api.init()` - initializes the presentation,
        `api.next()` - moves to next step of the presentation,
        `api.prev()` - moves to previous step of the presentation,
        `api.goto( idx | id | element, [duration] )` - moves the presentation to the step given by its index number
                id or the DOM element; second parameter can be used to define duration of the transition in ms,
                but it's optional - if not provided default transition duration for the presentation will be used.

    You can also simply call `impress()` again to get the API, so `impress().next()` is also allowed.
    Don't worry, it wont initialize the presentation again.

    For some example uses of this API check the last part of the source of impress.js where the API
    is used in event handlers.

-->

</body>
</html>

<!--

    Now you know more or less everything you need to build your first impress.js presentation, but before
    you start...

    Oh, you've already cloned the code from GitHub?

    You have it open in text editor?

    Stop right there!

    That's not how you create awesome presentations. This is only a code. Implementation of the idea that
    first needs to grow in your mind.

    So if you want to build great presentation take a pencil and piece of paper. And turn off the computer.

    Sketch, draw and write. Brainstorm your ideas on a paper. Try to build a mind-map of what you'd like
    to present. It will get you closer and closer to the layout you'll build later with impress.js.

    Get back to the code only when you have your presentation ready on a paper. It doesn't make sense to do
    it earlier, because you'll only waste your time fighting with positioning of useless points.

    If you think I'm crazy, please put your hands on a book called "Presentation Zen". It's all about
    creating awesome and engaging presentations.

    Think about it. 'Cause impress.js may not help you, if you have nothing interesting to say.

-->

<!--

    Are you still reading this?

    For real?

    I'm impressed! Feel free to let me know that you got that far (I'm @bartaz on Twitter), 'cause I'd like
    to congratulate you personally :)

    But you don't have to do it now. Take my advice and take some time off. Make yourself a cup of coffee, tea,
    or anything you like to drink. And raise a glass for me ;)

    Cheers!

-->
