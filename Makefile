.PHONY: serve
default: serve

serve:
	docker run \
		--rm \
		-ti \
		--entrypoint /usr/bin/python \
		-p 8080:8080 \
		-v "$(shell pwd)":/app \
		--workdir /app \
		python:2.7 -m SimpleHTTPServer 8080
